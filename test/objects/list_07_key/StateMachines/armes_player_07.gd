extends State


func state_enter_state(_msg := {}):
	owner.label.text = "armes"
	owner.star = "armes"
	owner.anim.play("armes_" + owner.stares)
	await get_tree().create_timer(0.4).timeout
	state_machine.transition_to("idle")


func state_physics_process(_delta):
	var dir_x = Input.get_axis("ui_a", "ui_d")
	var dir_y = Input.get_axis("ui_w", "ui_s")
		
	owner.velocity.x = dir_x * owner.SPEED
	owner.velocity.y = dir_y * owner.SPEED
	owner.move_and_slide()
	
	
