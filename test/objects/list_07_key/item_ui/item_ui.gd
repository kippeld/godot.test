extends CanvasLayer

@onready var slot = $item_gui

var is_open : bool = false
var index : bool = false

func _ready():
	pass # Replace with function body.


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if Input.is_action_just_released("ui_m"):
		if is_open:
			close()
			index = false
		else:
			open()
			index = true
			
func _ene_bar():
	if index:
		if Input.is_action_just_pressed("ui_d"):
			
			slot.item_max()
			
				
			#slot.item_ui_update()
			#slot.item_create()
				
		if Input.is_action_just_pressed("ui_a"):
			
			slot.item_min()
			
			
			#slot.item_ui_update()
			#slot.item_create()


func open() -> void:
	visible = true
	is_open = true
	get_tree().paused = true
	
func close() -> void:
	get_tree().paused = false
	visible = false
	is_open = false
