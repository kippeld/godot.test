extends Node

var laser := load("res://objects/list_03_laser/laser.tscn")

@onready var mark_01 := $Marker2D
@onready var timer_mark_01 := $"../Timer_mark_01"

func _ready():
	timer_mark_01.start()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(_delta):
	pass


func _on_timer_mark_01_timeout():
	var expo = laser.instantiate()
	get_tree().current_scene.add_child(expo)
	expo.global_position = mark_01.global_position
