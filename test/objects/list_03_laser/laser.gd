extends Area2D


var direction : Vector2 = Vector2.RIGHT
var speed : float = 200


func _process(_delta):
	translate(direction.normalized() * _delta * speed)

func _on_visible_on_screen_notifier_2d_screen_exited():
	queue_free()
