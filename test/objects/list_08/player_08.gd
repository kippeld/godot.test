extends CharacterBody2D


const SPEED = 300.0
const JUMP_VELOCITY = -400.0
const JUMP = 400

var gravity = ProjectSettings.get_setting("physics/2d/default_gravity")

@onready var anim := $AnimationPlayer
@onready var player := $Character0001
@onready var label := $Label

@export var star : StateMachinePlayer_08

var climb_bool : bool = false

var ray

func _ready():
	
	
	ray = $Ray.get_children()

func _on_climb_body_entered(body):
	climb_bool = true


func _on_climb_body_exited(body):
	climb_bool = false


func Detectar():
	for i in ray:
		if i.is_colliding():
			var obj = i.get_collider()
			
			if obj.is_in_group("plataforms"):
				#print("fff")
				if Input.is_action_just_pressed("ui_s"):
					
					obj.get_node("CollisionShape2D").rotation_degrees = 180
					await (get_tree().create_timer(0.1).timeout)
					obj.get_node("CollisionShape2D").rotation_degrees = 0
					
					
func Detectar_climb():
	for i in ray:
		if i.is_colliding():
			var obj = i.get_collider()
			
			if obj.is_in_group("plataforms_climb"):
				#print("fff")
				if Input.is_action_just_pressed("ui_s"):
					
					obj.get_node("CollisionShape2D").rotation_degrees = 180
					await (get_tree().create_timer(0.1).timeout)
					obj.get_node("CollisionShape2D").rotation_degrees = 0
					
					star.transition_to("climb")
