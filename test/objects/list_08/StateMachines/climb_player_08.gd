extends State

var climb_int = 80

func state_enter_state(_msg := {}):
	owner.label.text = "climb"

func state_process(_delta):
	
	if !owner.climb_bool:
		state_machine.transition_to("idle")
	
	if Input.is_action_pressed("ui_w") or Input.is_action_pressed("ui_s"):
		owner.anim.play("climb")
	else:
		owner.anim.stop()
	
	
	if Input.is_action_just_pressed("ui_a") or Input.is_action_just_pressed("ui_d"):
		state_machine.transition_to("jump")
		
	
	if Input.is_action_pressed("ui_w"):
		owner.velocity.y = -climb_int
	elif Input.is_action_pressed("ui_s"):
		owner.velocity.y = climb_int
	else:
		owner.velocity.y = lerpf(owner.velocity.y, 0, 0.4 )
	
	owner.move_and_slide()
	
	#owner.Detectar()	
	
