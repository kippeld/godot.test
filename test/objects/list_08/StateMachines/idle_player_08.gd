extends State

func state_enter_state(_msg := {}):
	#owner.anim.play("idle") #"idle")
	owner.label.text = "idle"
	pass
	
func state_process(_delta):
	owner.anim.play("idle")
	
	
	
	var _dir = Input.get_axis("ui_a", "ui_d")
	
	owner.move_and_slide()
	if not owner.is_on_floor():
		owner.velocity.y += owner.gravity * _delta
	
	if _dir != 0:
		state_machine.transition_to("run")
	elif !owner.is_on_floor():
		state_machine.transition_to("jump")
	elif Input.is_action_just_pressed("ui_accept"):
		state_machine.transition_to("jump", { Salto = true})
	elif owner.climb_bool and Input.is_action_just_pressed("ui_w"):
		state_machine.transition_to("climb")
	
	owner.Detectar()
	owner.Detectar_climb()
