extends State


func state_enter_state(_msg := {}):
	owner.label.text = "space"

func state_process(_delta):
	owner.anime.play("space")
	var _dir = Input.get_axis("ui_w", "ui_s")
	#var a = Input.is_action_pressed("ui_a")
	#var d = Input.is_action_pressed("ui_d")
	owner.velocity.x = 0
	owner.velocity.y = 0
	owner.move_and_slide()
	
	if _dir != 0:
		state_machine.transition_to("run")
	elif Input.is_action_pressed("ui_a"):
		state_machine.transition_to("right_left", {"right_left" : "left"})
	elif Input.is_action_pressed("ui_d"):
		state_machine.transition_to("right_left", {"right_left" : "right"})
	#elif d != 0:
	#	pass
	
	
