extends State

var ui : String 
var dir_red : float # -1, 1
var anim_play

func state_enter_state(_msg := {}):
	
	if _msg["right_left"] == "left":
		ui = "ui_a"
		dir_red = -1
		owner.label.text = "left"
		anim_play = "left"
	elif _msg["right_left"] == "right":
		ui = "ui_d"
		dir_red = 1
		owner.label.text = "right"
		anim_play = "right"
		
		
	

func state_process(_delta):
	owner.anime.play(anim_play)
	if !Input.is_action_pressed(ui):
		
		state_machine.transition_to("space")
	
	owner.velocity.x = dir_red * owner.SPEED #* _delta
	owner.move_and_slide()
