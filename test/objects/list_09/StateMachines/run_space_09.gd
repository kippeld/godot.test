extends State

func state_enter_state(_msg := {}):
	owner.label.text = "run"
	
func state_process(_delta):
	owner.anime.play("space")
	var dir = Input.get_axis("ui_w", "ui_s")
	
	owner.velocity.x = 0
	owner.velocity.y = dir * owner.SPEED
	owner.move_and_slide()
	
	if dir == 0:
		state_machine.transition_to("space")
	elif Input.is_action_pressed("ui_a"):
		state_machine.transition_to("right_left", {"right_left" : "left"})
	elif Input.is_action_pressed("ui_d"):
		state_machine.transition_to("right_left", {"right_left" : "right"})
