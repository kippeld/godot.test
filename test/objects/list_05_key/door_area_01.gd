extends Area2D

var index : bool = false
@onready var label := $Label
var label_e : String = "[e] button"

@export var scale_bar : String

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.


func _process(delta):
	if index:
		if Input.is_action_just_released("ui_e"):
			get_tree().change_scene_to_file(scale_bar)


func _on_body_entered(body):
	if body is Player_04:
		index = true
		label.text = label_e


func _on_body_exited(body):
	if body is Player_04:
		index = false
		label.text = ""
