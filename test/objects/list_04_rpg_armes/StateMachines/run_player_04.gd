extends State

var red : String

func state_enter_state(_msg := {}):
	owner.label.text = "run"
	owner.star = "run"
	

func star_red() -> void:
	
	if owner.velocity.x < 0:
		owner.stares = "a"	
	elif owner.velocity.x > 0:
		owner.stares = "d"
	elif owner.velocity.y < 0:
		owner.stares = "w"
	elif owner.velocity.y > 0:
		owner.stares = "s"
		
	

func state_physics_process(_delta):
	
	
	var dir_x = Input.get_axis("ui_a", "ui_d")
	var dir_y = Input.get_axis("ui_w", "ui_s")
	
	
	
		
	owner.velocity.x = dir_x * owner.SPEED
	owner.velocity.y = dir_y * owner.SPEED
	owner.move_and_slide()
	
	self.star_red()
	owner.anim.play("run_" + owner.stares)
	
	
	if dir_x == 0 and dir_y == 0:
		state_machine.transition_to("idle")
	elif Input.is_action_just_pressed("ui_space"):
		state_machine.transition_to("armes")
	
