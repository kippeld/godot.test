extends State

func state_enter_state(_msg := {}):
	owner.label.text = "idle"
	owner.star = "idle"
	
	
func state_process(_delta):
	
	var _dir_x = Input.get_axis("ui_a", "ui_d")
	var _dir_y = Input.get_axis("ui_w", "ui_s")
	
	owner.move_and_slide()	
	owner.anim.play( "idle_" + owner.stares)

	if _dir_x != 0 or _dir_y != 0:
		state_machine.transition_to("run")
	if Input.is_action_just_pressed("ui_space"):
		state_machine.transition_to("armes")

